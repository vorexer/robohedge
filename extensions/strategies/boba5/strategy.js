var z = require('zero-fill')
  , n = require('numbro')
  , sma = require('../../../lib/sma')
  , stddev = require('../../../lib/stddev')
  , defineTrend = require('../../../lib/trends')
  , defineLevel = require('../../../lib/levels')
  , Phenotypes = require('../../../lib/phenotype')

module.exports = {
  name: 'boba5',
  description: 'Buy when (Signal ≤ Lower Boba5 (LBB) and again > LBB) and sell when (Signal ≥ Upper Boba5 and again < UBB).',

  getOptions: function () {
    this.option('min_periods', 'period length, same as --period_length', Number, 800)
    this.option('period', 'period length, same as --period_length', String, '2m')
    this.option('period_length', 'period length, same as --period', String, '2m')
    this.option('bb5_min_summ', 'trade if have more coins or currency', Number, 10)
    this.option('bb5_size', 'bb5 period size', Number, 400)
    this.option('bb5_time', 'times of standard deviation between the upper band and the moving averages', Number, 3.3)
    this.option('bb5_cross_pct', 'pct the current price should be greate upper/lower bound before change level', Number, 0.5)
    this.option('bb5_fibo', 'multiplier for Fibo levels', Number, 0.618)
    this.option('bb5_level_width', 'width of the level in pct of the current price need for trades', Number, 0.7)
    this.option('bb5_level_down_sell', 'number periods reverce trend for sale', Number, 1)
    this.option('bb5_level_up_buy', 'number periods reverce trend for buy', Number, 1)
    this.option('bb5_signal_down_sell', 'sell if price drops below this % of max price', Number, 2)
    this.option('bb5_signal_up_buy', 'buy if price surges above this % of min price', Number, 2)
    this.option('bb5_trend_periods', 'number of periods for trend calc', Number, 10)
    this.option('bb5_neutral_trend', 'neutral trend value', Number, 0.2)
    this.option('bb5_debug', 'log on/0ff', Boolean, false)
  },

  calculate: function (s) {
    // calculate sma
    sma(s, 'sma', s.options.bb5_size, 'close')
    // calculate Stdev
    stddev(s, 'stdev', s.options.bb5_size, 'close')
  },

  onPeriod: function (s, cb) {
    if (s.period.stdev) {
      defineLevel(s)
      defineTrend(s)
      if (s.options.bb5_debug) console.log(s.bb5)
      if (s.bb5.signal === 'none') {
        //  Define moment for start buying or selling
        if (s.balance.currency > s.options.bb5_min_summ) { // Define balance of currency 
          //	Buy logic
          if (s.bb5.level == -5 ||
            (s.bb5.level - s.bb5.minLevel >= s.options.bb5_level_up_buy)) {
            s.bb5.signal = 'buy'
            s.bb5.minPrice = s.period.close
          }
        }
        if (s.balance.asset * s.period.close > s.options.bb5_min_summ) { // Define balance of coins 
          // Sell logic
          if ((s.bb5.level == 5) ||
            (s.bb5.maxLevel - s.bb5.level >= s.options.bb5_level_down_sell)) {
            s.bb5.signal = 'sell'
            s.bb5.maxPrice = s.period.close
          }
        }
      } else if (!s.api_order && s.bb5.signal === 'buy' && ( s.bb5.level > s.bb5.lastLevel ||
      s.period.close > adjust_by_pct(s.options.bb5_signal_up_buy, s.bb5.minPrice) &&
      s.bb5.trend != 'down' && s.bb5.levelWidth)) {
        //  moment (cross level or min price) for buying
        s.signal = 'buy'
        s.bb5.maxLevel = s.bb5.level
        s.bb5.minLevel = s.bb5.level
        s.bb5.signal = 'none'
      } else if (!s.api_order && s.bb5.signal === 'sell' && ( s.bb5.level < s.bb5.lastLevel ||
      s.period.close < adjust_by_pct(-s.options.bb5_signal_down_sell, s.bb5.maxPrice) && s.bb5.trend != 'up')) {
        //  moment (cross level or max price) for selling
        s.signal = 'sell'
        s.bb5.minLevel = s.bb5.level
        s.bb5.minLevel = s.bb5.level
        s.bb5.signal = 'none'
      }
      s.bb5.lastLevel = s.bb5.level
    }
    cb()
  },

  onReport: function (s) {
    var cols = []
    if (s.period.stdev) {
      let upperBound = s.period.sma + adjust_by_pct(s.options.bb5_cross_pct, s.period.stdev * s.options.bb5_time)
      let lowerBound = s.period.sma - adjust_by_pct(s.options.bb5_cross_pct, s.period.stdev * s.options.bb5_time)
      var color = 'grey'
      if (s.period.close > upperBound) {
        color = 'green'
      } else if (s.period.close < lowerBound) {
        color = 'red'
      }
      cols.push(z(2, n(s.bb5.level).format('+0'), ' ').cyan)
      cols.push(z(7, n(s.period.sma).format('000.000'), ' ')[color])
      color = 'grey'
      if (s.bb5.trend === 'up') {
        color = 'green'
      } else if (s.bb5.trend === 'down') {
        color = 'red'
      }
      cols.push(z(8, n(s.bb5.trend_value).format('0.000000').substring(0, 7), ' ')[color])
      cols.push(z(8, n(lowerBound).format('0.000000').substring(0, 7), ' ').cyan)
      cols.push(z(8, n(upperBound).format('0.000000').substring(0, 7), ' ').cyan)
    } else {
      cols.push('   ')
    }
    return cols
  },

  phenotypes: {
    // -- common
    period_length: Phenotypes.RangePeriod(2, 30, 'm'),
    profit_stop_enable_pct: Phenotypes.RangeFloat(0, 15),
    profit_stop_pct: Phenotypes.RangeFloat(0, 3),
    // -- strategy
    bb5_size: Phenotypes.Range(5, 800),
    bb5_cross_pct: Phenotypes.RangeFloat(-3, 3),
    bb5_signal_up_buy: Phenotypes.RangeFloat(-3, 3),
    bb5_signal_down_sell: Phenotypes.RangeFloat(-3, 3),
    bb5_level_down_sell: Phenotypes.Range0(0, 3),
    bb5_level_up_buy: Phenotypes.Range0(0, 3),
    bb5_level_width: Phenotypes.RangeFloat(0, 1),
    bb5_trend_periods: Phenotypes.Range(5, 30),
    bb5_neutral_trend: Phenotypes.RangeFloat(0, 1)
  }
}

var adjust_by_pct = function (pct, n) {
  return n * (pct / 100 + 1)
}
