var z = require('zero-fill')
  , n = require('numbro')
  , ktrend = require('../../../lib/ktrend')
  , Phenotypes = require('../../../lib/phenotype')

module.exports = {
  name: 'ktrend',
  description: 'Buy when (KTREND < -0.5) and sell when (KTREND > 0.5).',

  getOptions: function () {
    this.option('period', 'period length, same as --period_length', String, '3m')
    this.option('period_length', 'period length, same as --period', String, '3m')
    this.option('min_periods', 'min. number of history periods', Number, 40)
    this.option('ktrend_period', 'number of periods for the KTREND', Number, 10)
    this.option('ktrend_koeff', 'multiplier for 30m period', Number, 10)
    this.option('up_trend_threshold', 'threshold to trigger a buy signal', Number, 0.5)
    this.option('down_trend_threshold', 'threshold to trigger a sold signal', Number, 0.4)
    this.option('signal_pct', 'price deviation to trigger a trade signal', Number, 0.4)
  },

  calculate: function (s) {

    // compute TREND
    ktrend(s, 'ktrend', s.options.ktrend_period, s.options.ktrend_koeff)
  },

  onPeriod: function (s, cb) {
    if (typeof s.period.ktrend === 'number') {
      console.log('----------------------------------------------------')
      if (!s.signal && s.period.ktrend < -s.options.down_trend_threshold) s.pre_signal = 'buy'
      else if (!s.signal && s.period.ktrend > s.options.up_trend_threshold) s.pre_signal = 'sell'
      if (!s.api_order && s.pre_signal === 'buy') {
        s.min_price = Math.min(s.min_price, s.period.close)
        if (s.period.close > s.min_price * (1 + s.options.signal_pct/100)) {  
          s.signal = 'buy'
          s.pre_signal = ''
        }
      } else if (!s.api_order && s.pre_signal === 'sell') {
        s.max_price = Math.max(s.max_price, s.period.close)
        if (s.period.close < s.max_price / (1 + s.options.signal_pct/100)) {  
          s.signal = 'sell'
          s.pre_signal = ''
        }
      } else {
        s.signal = null  // hold
        s.min_price = 1000000
        s.max_price = 0
      }
    }
    cb()
  },

  onReport: function (s) {
    var cols = []
    if (typeof s.period.ktrend === 'number') {
      var color = 'grey'
      if (s.period.ktrend > s.options.up_trend_threshold) {
        color = 'green'
      }
      else if (s.period.ktrend < -s.options.down_trend_threshold) {
        color = 'red'
      }
      cols.push(z(8, n(s.period.ktrend).format('+00.0000'), ' ')[color])
      cols.push(z(8, n(s.period.overbought_rsi).format('00'), ' ').cyan)
    }
    else {
      cols.push('         ')
    }
    return cols
  },

  phenotypes: {
    // -- common
    period_length: Phenotypes.RangePeriod(2, 30, 'm'),
    profit_stop_enable_pct: Phenotypes.Range0(1, 10),
    profit_stop_pct: Phenotypes.Range(0,5),

    // -- strategy
    ktrend_period: Phenotypes.Range(2, 20),
    ktrend_koeff: Phenotypes.Range(1, 15),
    up_trend_threshold: Phenotypes.RangeFloat(-1, 3),
    down_trend_threshold: Phenotypes.RangeFloat(-1, 3),
    signal_pct: Phenotypes.RangeFloat(-1, 5)
  }
}

