// BCH settings

var c = module.exports = {}

c.strategy = 'ktrend'
c.selector = 'bitfinex.BCH-USD'
c.period = '4m'
c.ktrend_period = 10
c.min_periods = 40
c.ktrend_koeff = 10
c.down_trend_threshold = 1.2
c.up_trend_threshold = 2
c.signal_pct = 0.5
c.max_slippage_pct = 10
