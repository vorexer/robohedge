#!/bin/bash
# Это Родитель бота
# Рабочий каталог
WATCH_DIR="/home/ubuntu/zenbot/log"

# В этот файл будем писать ход выполнения скрипта
#LOG=

# В этот файл будут попадать ошибки при работе скрипта
#ERR_LOG=

# Создаем файлы логов
#touch ${LOG}
#touch ${ERR_LOG}

	# переходим в корень, что бы не блокировать фс

# Перенаправляем стандартный вывод, вывод ошибок и стандартный ввод
exec 6>&1  	# Связать дескр. #6 со stdout. сохранем stdout
exec > "${WATCH_DIR}/bot_file_work.log"
exec 2> "${WATCH_DIR}/bot_file_error.log"
#exec < /dev/null
cd /home/ubuntu/zenbot
# ./zenbot.sh trade --conf peu_buy.js
echo $1
./zenbot.sh $1 
exec 1>&6 6>&-      # Восстановить stdout и закрыть дескр. #6.

exit 0
