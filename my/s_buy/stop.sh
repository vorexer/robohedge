#!/bin/bash
# Это скрипт останова всех ботов
# Рабочий каталог
WATCH_DIR="/home/ubuntu/zenbot/log"

# В этот файл будем писать ход выполнения скрипта
#LOG=

# В этот файл будут попадать ошибки при работе скрипта
#ERR_LOG=

# Создаем файлы логов
#touch ${LOG}
#touch ${ERR_LOG}

	# переходим в корень, что бы не блокировать фс

# Перенаправляем стандартный вывод, вывод ошибок и стандартный ввод
exec 6>&1  	# Связать дескр. #6 со stdout. сохранем stdout
exec > "${WATCH_DIR}/stop_work.log"
exec 2> "${WATCH_DIR}/stop_error.log"
exec < /dev/null
ps axww | grep -e 'zenbot' | grep -v 'grep' | awk -F' ' '{print $1}'>tokill
while read line
do
  kill -s 9 $line
done <tokill
echo ''>/home/ubuntu/zenbot/flag.run
exec 1>&6 6>&-      # Восстановить stdout и закрыть дескр. #6.

exit 0
