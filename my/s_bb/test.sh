#!/bin/bash
# Это скрипт запуска бота в режиме теста 
# Рабочий каталог
WATCH_DIR="/home/ubuntu/zenbot/log"

# В этот файл будем писать ход выполнения скрипта
#LOG=

# В этот файл будут попадать ошибки при работе скрипта
#ERR_LOG=

# Создаем файлы логов
#touch ${LOG}
#touch ${ERR_LOG}

	# переходим в корень, что бы не блокировать фс

# Перенаправляем стандартный вывод, вывод ошибок и стандартный ввод
exec 6>&1  	# Связать дескр. #6 со stdout. сохранем stdout
exec > "${WATCH_DIR}/test_work.log"
exec 2> "${WATCH_DIR}/test_error.log"
exec < /dev/null
cd /home/ubuntu/zenbot
rm -rf ./simulations/*.html
echo "test --peu_bb --days=1" $1 $2 $3 $4>flag.run 
./zenbot.sh sim --conf peu_bb.js --days=1 $1 $2 $3 $4
mv ./simulations/*.html ./simulations/test.html
echo "">flag.run
cd /
exec 1>&6 6>&-      # Восстановить stdout и закрыть дескр. #6.
exit 0
