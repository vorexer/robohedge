var c = module.exports = {}

//peu - polonex ETH/USD trading

//Custom parameters

c.selector = 'poloniex.ETH-USDT'

//Simulation parameters

c.days = 2
c.asset_capital=0
c.currency_capital=300

//Strategy parameters

c.strategy =  'macd'
//    c.period=<value>  period length (default: 1h)
c.period='2m'
//    c.min_periods=<value>  min. number of history periods (default: 52)
c.min_periods=40
//    c.ema_short_period=<value>  number of periods for the shorter EMA (default: 12)
c.ema_short_period=3
//    c.ema_long_period=<value>  number of periods for the longer EMA (default: 26)
c.ema_long_period=9
//    c.signal_period=<value>  number of periods for the signal EMA (default: 9)
c.signal_period=5
//    c.up_trend_threshold=<value>  threshold to trigger a buy signal (default: 0)
c.up_trend_threshold=2.9
//    c.down_trend_threshold=<value>  threshold to trigger a sold signal (default: 0)
c.down_trend_threshold=2.9
//    c.overbought_rsi_periods=<value>  number of periods for overbought RSI (default: 25)
//    c.overbought_rsi=<value>  sold when RSI exceeds this value (default: 70)
c.overbought_rsi=75

//Defaults (to change - delete comment sign // and input real value)

// Optional stop-order triggers:

// sell if price drops below this % of bought price (0 to disable)
//c.sell_stop_pct = 0
// buy if price surges above this % of sold price (0 to disable)
//c.buy_stop_pct = 4
// enable trailing sell stop when reaching this % profit (0 to disable)
c.profit_stop_enable_pct = 5
// maintain a trailing stop this % below the high-water mark of profit
c.profit_stop_pct = 1

// Order execution rules:

// avoid trading at a slippage above this pct
//c.max_slippage_pct = 5
// buy with this % of currency balance
//c.buy_pct = 99
// sell with this % of asset balance
//c.sell_pct = 99
// ms to adjust non-filled order after
//c.order_adjust_time = 30000
// avoid selling at a loss below this pct
//c.max_sell_loss_pct = 25
// ms to poll order status
//c.order_poll_time = 5000
// ms to wait for settlement (after an order cancel)
//c.wait_for_settlement = 5000
// ms to wait for settlement (after a funds on hold error)
//c.wait_more_for_settlement = 60000
// % to mark up or down price for orders
//c.markup_pct = 0

// Misc options:

// default # days for backfill and sim commands
//c.days = 14
// ms to poll new trades at
//c.poll_trades = 30000
// amount of currency to start simulations with
//c.currency_capital = 1000
// amount of asset to start simulations with
//c.asset_capital = 0
// for sim, reverse time at the end of the graph, normalizing buy/hold to 0
//c.symmetrical = false
// number of periods to calculate RSI at
//c.rsi_periods = 14
// period to record balances for stats
//c.balance_snapshot_period = '15m'
