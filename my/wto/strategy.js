var z = require('zero-fill')
  , n = require('numbro')

module.exports = function container (get, set, clear) {
  return {
    name: 'wto',
    description: 'Buy when (EMA - last(EMA) > 0) and sell when (EMA - last(EMA) < 0).',

    getOptions: function () {
      this.option('on_start', 'Indicate start', Boolean, false)
//	Common parameters				
      this.option('period', 'period length', String, '2m')
      this.option('min_periods', 'min. number of history periods', Number, 240)
      this.option('trend_ema', 'number of periods for long ema', Number, 20)
      this.option('neutral_ema', 'avoid trades if abs(trend_sma) under this float (0 to disable, "auto" for a variable filter)', Number, 0.03)
      this.option('bb_period', 'numbers of periods for bb', Number, 30)
      this.option('bb_w', 'koeff for width bb', Number, 2.0)
      this.option('oversold_rsi_periods', 'number of periods for oversold RSI', Number, 20)
//	Price parameters
      this.option('abs_max_price', 'historical max price', Number, 408.13)
      this.option('abs_min_price', 'historical min price', Number, 0)
      this.option('abs_gamma', 'Const for Fibo abs_force', Number, 5)
      this.option('week_max_price', 'week max price', Number, 224.65)
      this.option('week_min_price', 'week min price', Number, 189.60)
      this.option('week_gamma', 'Const for Fibo week_force', Number, 10)
      this.option('long_periods', 'Number periods for long_price indicator', Number, 10)
	
//	Options for buying	
      this.option('buy_bbw', 'if min profit < this value - no ', Number, 0.1)
      this.option('min_curr', 'buy if have more currency', Number, 50)
//	Options for selling	
      this.option('err_pct', 'err control if price deviate by this pct from trade price', Number, 2.0)
      this.option('sell_bbw', 'if min profit < this value - no selling', Number, 0.1)
      this.option('min_coin', 'sell if have more coins', Number, 0.2)
   },

    calculate: function (s) {
      	var bbw, diff
			if (!s.options.on_start) {
// Initial params
				s.trend_ema = s.options.trend_ema
				s.bb_period = s.options.bb_period
				s.dev_ema = s.options.neutral_ema
				s.buy_pct = 1.0 + s.options.buy_bbw / 100
				s.err_pct = 1.0 + s.options.err_pct / 100
				s.sell_bbw = s.options.sell_bbw
				s.min_coin = s.options.min_coin
				s.min_curr =s.options.min_curr
				s.long_periods = s.options.long_periods
//	Initial values
				s.max_ind = 0 
	        	s.min_ind = 0
		      s.max_price = s.period.close
	        	s.min_price = s.period.close
				s.buy_price = s.period.close * s.buy_pct
				s.sell_price = s.period.close / s.sell_pct
				s.up_bb = false
				s.under_bb = false
				s.start_buy = false
				s.start_sell = false
				s.long_ind = 0
				s.long_price = [0, 0, 0]
				s.bbw = 0
				s.bb_wto = 0
				s.wto_start = 0
				s.long_rsi = 0
		      s.options.on_start = true
			}
// Calculate indicators
//			process.exit()
			s.period.hlc3 = (s.period.high + s.period.low + s.period.close) / 3
      	get('lib.sma')(s, 'sma', s.bb_period)
// EMA, SMA, RSI
			get('lib.ema')(s, 'long_ema', s.trend_ema)
			s.options.rsi_periods = s.options.oversold_rsi_periods
			get('lib.rsi')(s, 'my_rsi', s.options.rsi_periods)
			get('lib.sma')(s, 'long_rsi', 60, 'my_rsi')
			s.long_rsi = s.period.long_rsi - 50
// Long period prices	
			s.long_ind++
			if (s.long_ind = s.long_periods) {
				s.long_ind = 0
				s.long_price.pop()
				s.long_price.unshift(s.period.close)
			}
//       	if (s.period.long_ema && s.lookback[2] && s.lookback[2].long_ema) {
       	if (s.period.sma && s.lookback[s.bb_period] && s.lookback[s.bb_period].sma) {
//        		s.period.ema_rate = (s.period.sma - s.lookback[0].sma) / s.lookback[0].sma * 100
        		s.period.ema_rate = (s.lookback[0].sma - s.lookback[2].sma) / s.lookback[2].sma * 50 
// Bollinger Bands		
				get('lib.stddev')(s, 'bb', s.bb_period, 'close')   			
   			s.bbw = s.period.bb * s.options.bb_w
				s.w_bb = s.bbw * 100 / s.period.sma		      
// My Indicator
// WTO 
//			s.period.hlc3 = s.period.close
				get('lib.ema')(s, 'esa', s.bb_period/4, 'hlc3')
				s.period.ds = Math.abs(s.period.hlc3 - s.period.esa) 
				get('lib.ema')(s, 'dema', s.bb_period/4, 'ds')
				s.period.ci = (s.period.hlc3 - s.period.esa)/s.period.dema/0.015
				get('lib.ema')(s, 'my_ind', s.bb_period/2, 'ci')
//				s.period.my_ind = s.period.wto*(1+s.period.w_bb/50)
				get('lib.sma')(s, 'sma_ind', s.bb_period*2, 'my_ind')
   	      get('lib.stddev')(s, 'ind_dev', s.bb_period*2, 'my_ind')
//				s.bb_wto = s.period.my_ind - s.period.sma_ind + s.period.ind_dev * 1.62
				s.bb_wto = s.period.my_ind
//				s.wto_down = 0
				s.wto_down =  s.period.sma_ind - s.period.ind_dev * 1.62
//				s.wto_up = s.period.ind_dev*3.64  
				s.wto_up = s.period.sma_ind + s.period.ind_dev * 1.62  
        		s.period.ind_rate = (s.period.sma_ind - s.lookback[0].sma_ind) / (s.lookback[0].sma_ind + 80) * 100
			}
 },

	onPeriod: function (s, cb) {
		var in_coin = in_curr = false
  		var go_buy = false
      var go_sell = false
		var local_up  = local_down = false
		var global_up = global_down = false

// Define balances 
		if (s.balance.asset > s.min_coin) in_coin = true
		if (s.balance.currency > s.min_curr) in_curr = true
//	Define long_ema trend
		if (s.period.ind_rate > s.dev_ind) ind_up = true
		else if (s.period.ind_rate < -1 * s.dev_ind) ind_down = true
//	Define long_ema trend
		if (s.period.ema_rate > s.dev_ema) local_up = true
		else if (s.period.ema_rate < -1 * s.dev_ema) local_down = true
// Define min max ind
		if (s.bb_wto > s.max_ind) s.max_ind = s.bb_wto
		if (s.bb_wto < s.min_ind) s.min_ind = s.bb_wto
// Define min max price
		if (s.period.close > s.max_price) s.max_price = s.period.close
		if (s.period.close < s.min_price) s.min_price = s.period.close
//	Define out_side bb & trade for correct mistake
		if (!s.up_bb && s.period.close >=  s.period.sma + s.bbw) {
			if (in_curr && !local_down) go_buy = true  //Correct mistakes
			s.up_bb = true
			s.under_bb = false
			s.start_trade = false
			s.start_buy = false
			s.max_ind = s.bb_wto
			s.max_price = s.period.close
		}
		else if (!s.under_bb && s.period.close <= s.period.sma - s.bbw) {
			s.under_bb =true 
			s.up_bb = false
			s.start_trade = false
			s.start_sell = false
			s.min_ind = s.bb_wto
			s.min_price = s.period.close
		}
		else { 
//	All trades is making inside bb 				  
//	Define moment for sell		
			if (s.up_bb) {
//				if (in_curr & s.period.close > s.sell_price * s.err_pct) go_buy = true //Correct mistakes
				if (in_curr) {
					if (s.w_bb < 0.3 && local_up || s.w_bb > s.wto_up && !local_down) go_buy = true  //Correct mistakes
					if (s.bb_wto < s.wto_down && !s.start_trade && local_up) { //Correct mistakes
						s.start_trade = true
						s.min_ind = s.bb_wto
					}
					if (!s.start_buy && s.start_trade && s.bb_wto > s.min_ind && s.bb_wto < s.wto_down) {
						go_buy = true
						s.min_price=s.period.close
						s.up_bb = false
					}
				}
			 	else if (in_coin) {
					if (s.w_bb < 0.55 & !local_down) {
						s.start_trade = false
						s.start_sell = false
					}
					if (!s.start_trade) {
						if (s.bb_wto > s.wto_up  && !local_up && s.w_bb > 0.55) {
							s.start_trade = true
							s.max_ind = s.bb_wto
						}
					} else if (local_up) s.start_trade = false
					if (!s.start_sell && s.start_trade && s.bb_wto < s.max_ind && !local_up) {
					 	s.start_sell = true
//						s.max_price=s.period.close
					}
				}
			}
//	Define moment for buy
//	If rise after min price or avoid loses after selling
			else if (s.under_bb) {
				if (in_coin) {
					if (s.w_bb < 0.4 & local_down  || s.w_bb < s.wto_down && !local_up && s.w_bb > 0.55) go_sell = true  //Correct mistakes
					if (s.bb_wto > s.wto_up && !s.start_trade && local_down) { //Correct mistakes
						s.start_trade = true
						s.max_ind = s.bb_wto
					}
//					if (!s.start_sell && s.start_trade && s.bb_wto < s.max_ind && s.bb_wto > s.wto_up) {
					if (!s.start_sell && s.start_trade && s.bb_wto < s.max_ind) {
						go_sell = true
						s.max_price=s.period.close
						s.under_bb = false
					}
				}
				else if (in_curr) {
					if (!local_up && s.w_bb < 0.55) {
						s.start_trade = false
						s.start_buy = false
					}
					if (!s.start_trade) {
						if (s.bb_wto < s.wto_down && !local_down) {
							s.start_trade = true
							s.min_ind = s.bb_wto
						}
					}
					else if (local_down) s.start_trade = false
					if (!s.start_buy && s.start_trade && s.w_bb > 0.6 && s.bb_wto > s.min_ind && !local_down) {
						s.start_buy = true
						s.min_price=s.period.close
					}
				}
			}
//			if (s.start_sell && s.period.close < s.max_price/s.buy_pct && s.bb_wto - s.period.sma_ind < 20) go_sell = true
			if (in_coin && s.start_sell && s.period.close < s.max_price/s.buy_pct) go_sell = true
			else if (in_curr && s.start_buy && s.period.close > s.min_price*s.buy_pct) go_buy = true
		}
//		if (in_curr) console.log('in_curr '+ s.start_trade + s.start_buy+ ' close '+ s.period.close+' min '+ (s.min_price*s.buy_pct) +' w '+ s.w_bb)
//		else if (in_coin) console.log('in_coin '+ s.start_trade + s.start_sell+ ' close '+ s.period.close+ ' max ' + (s.max_price/s.buy_pct) + ' w '+ s.w_bb)
		console.log('data1 '+(s.period.sma-s.bbw) +' '+s.period.sma +' '+s.period.close+' '+(s.period.sma+s.bbw))
		console.log('data2 '+s.wto_down +' '+s.period.sma_ind +' '+s.bb_wto+' '+s.wto_up)
// Trades
		if (!local_down && go_buy) {
			s.signal = 'buy'
			s.start_buy = false
			s.start_sell = false
			s.start_trade = false
			s.min_ind = s.bb_wto
			s.max_ind = s.bb_wto
			s.min_price = s.period.close
			s.max_price = s.period.close
			s.buy_price = s.period.close
		}
		if  (!local_up && go_sell ) {
			s.signal = 'sell'
			s.start_sell = false
			s.start_buy = false
			s.start_trade = false
			s.min_ind = s.bb_wto
			s.max_ind = s.bb_wto
			s.min_price = s.period.close
			s.max_price = s.period.close
			s.buy_price = s.period.close
			s.sell_price = s.period.close
		}
		cb()
   },

    onReport: function (s) {
      var cols = []
      var color = 'grey'
//		var bb_wto = s.period.sma_wto + s.period.wto_dev * (s.period.sma_wto > 0 ?  1 : -1)
//		var bb_wto = s.period.my_ind //+ s.period.ind_dev * (s.period.sma_ind > 0 ?  2 : -2) 
//		cols.push(z(6, n(s.wto_start).format('00.0'), ' ').white)
//		cols.push(z(7, n((s.up_bb ? 50 : -50) + s.abs_force + s.week_force).format('00.00'), ' ').white)
//			cols.push(z(6, n(s.long_rsi).format('00.0'), ' ').white)
			cols.push(z(6, n(s.period.ind_dev).format('00.0'), ' ').white)
//        	if (s.period.ema_rate > s.dev_ema) cols.push(z(8, n(s.period.close).format('000.00'), ' ').green)
//        	else if (s.period.ema_rate < s.dev_ema * -1) cols.push(z(8, n(s.period.close).format('000.00'), ' ').red)
//       	else cols.push(z(8, n(s.period.close).format('000.00'), ' ').grey)

       	cols.push(z(6, n(s.wto_down).format('00.0'), ' ').white)
			cols.push(z(6, n(s.period.sma_ind).format('00.0'), ' ').white)
       	cols.push(z(6, n(s.bb_wto).format('00.0'), ' ').white)
       	cols.push(z(6, n(s.wto_up).format('00.0'), ' ').white)
        	if (s.period.ema_rate > s.dev_ema) cols.push(z(7, n(s.period.ema_rate).format('0.000'), ' ').green)
        	else if (s.period.ema_rate < s.dev_ema * -1) cols.push(z(7, n(s.period.ema_rate).format('0.000'), ' ').red)
       	else cols.push(z(7, n(s.period.ema_rate).format('0.000'), ' ').grey)
//			cols.push(z(8, n(s.period.bb_up_rate).format('0.0000'), ' ').green)
//			cols.push(z(8, n(s.period.sma_rate).format('0.0000'), ' ').grey)
//			cols.push(z(8, n(s.period.bb_down_rate).format('0.0000'), ' ').red)
//			if (s.start_buy) cols.push(z(6, n(s.min_ind).format('00.0'), ' ').green)
//			else if (s.start_sell)  cols.push(z(6, n(s.max_ind).format('00.0'), ' ').red)
       	if (s.under_bb) cols.push(z(6, n(s.w_bb).format('0.00'), ' ').red)
        	else if(s.up_bb) cols.push(z(6, n(s.w_bb).format('0.00'), ' ').green)
        	else cols.push(z(6, n(s.w_bb).format('0.00'), ' ').white)
       	if (s.under_bb) cols.push(z(6, n(s.period.ind_rate).format('0.00'), ' ').red)
        	else if (s.up_bb) cols.push(z(6, n(s.period.ind_rate).format('0.00'), ' ').green)
        	else cols.push(z(6, n(s.period.ind_rate).format('0.00'), ' ').white)

     return cols
    }
  }
}
