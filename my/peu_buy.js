var c = module.exports = {}

//peu - polonex ETH/USD trading

c.selector='poloniex.ETH-USDT'

//Simulation parameters

//c.days=2
//c.asset_capital=0
//c.currency_capital=1000

//Strategy parameters

c.strategy='auto_buy'
//c.period='52m'
//c.trend_ema=21
//c.neutral_rate = 0.056
//c.oversold_rsi=20

//Defaults (to change - delete comment sign // and input real value)

// Optional stop-order triggers:

// sell if price drops below this % of bought price (0 to disable)
//c.sell_stop_pct = 0
// buy if price surges above this % of sold price (0 to disable)
//c.buy_stop_pct = 0.6
// enable trailing sell stop when reaching this % profit (0 to disable)
//c.profit_stop_enable_pct = 0
// maintain a trailing stop this % below the high-water mark of profit
//c.profit_stop_pct = 0

// Order execution rules:

// avoid trading at a slippage above this pct
//c.max_slippage_pct = 5
// buy with this % of currency balance
//c.buy_pct = 99
// sell with this % of asset balance
//c.sell_pct = 99
// ms to adjust non-filled order after
//c.order_adjust_time = 10000
// avoid selling at a loss below this pct
//c.max_sell_loss_pct = 25
// ms to poll order status
//c.order_poll_time = 5000
// ms to wait for settlement (after an order cancel)
//c.wait_for_settlement = 5000
// ms to wait for settlement (after a funds on hold error)
//c.wait_more_for_settlement = 60000
// % to mark up or down price for orders
//c.markup_pct = 0

// Misc options:

// default # days for backfill and sim commands
//c.days = 14
// ms to poll new trades at
//c.poll_trades = 30000
// amount of currency to start simulations with
//c.currency_capital = 1000
// amount of asset to start simulations with
//c.asset_capital = 0
// for sim, reverse time at the end of the graph, normalizing buy/hold to 0
//c.symmetrical = false
// number of periods to calculate RSI at
//c.rsi_periods = 14
// period to record balances for stats
//c.balance_snapshot_period = '15m'
