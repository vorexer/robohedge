// BCH settings

var c = module.exports = {}

c.selector = 'bitfinex.ETH-USD'
c.strategy = 'noop'
c.period = '2m'
c.min_period = 30
