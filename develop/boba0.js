'use strict'
var sheeftR = 3;

//*** Events functions  ***//
/*
function doGet() {
  //  BitfinexCandles2(); //  do nothing on load book
}
*/

function onOpen() {
  var spreadsheet = SpreadsheetApp.getActive();
  var menuItems = [{
    name: '1. Get Bitfinex data',
    functionName: 'loadData1'
  },
  {
    name: '2. Reload Candels',
    functionName: 'calcPrintCandels1'
  },
  {
    name: '3. Reload Indicators',
    functionName: 'calcPrintInd1'
  },
  {
    name: '4. Reload Trades',
    functionName: 'autoTrade3'
  },
  {
    name: 'Update Bitfinex coins',
    functionName: 'getSupprotedSymbols'
  }
  ];
  spreadsheet.addMenu('BOBA', menuItems);
}

//*** Main auto trade function ***//

function main() {
  var params = paramsFromMain();
  var stopDATE = Unixtime(params.DATE > Date.now() ? Date.now() : params.DATE);
  var objA = getBitfinexData(params.A, stopDATE, params.LIM);
  var objB = getBitfinexData(params.B, stopDATE, params.LIM);
  var sData = sortData1(objA, objB, stopDATE, params.LIM);
  objA = null;
  objB = null;
  printInput1(sData);
  var candels = calcPrintCandels1(sData, params.INT);
  sData = null;
  var indexes = calcPrintInd(candels, params);
  candels = null;
  autoTrade(indexes, params);
  return;
};

function loadData1() {
  var params = paramsFromMain();
  var stopDATE = Unixtime(params.DATE > Date.now() ? Date.now() : params.DATE);
  var objA = getBitfinexData(params.A, stopDATE, params.LIM);
  var sData = sortData1(objA, stopDATE, params.LIM);
  objA = null;
  printInput1(sData);
  //  var candels = calcPrintCandels(sData, params.INT);
  sData = null;
  //  var indexes = calcPrintInd(candels, params);
  // candels = null;
  //  autoTrade(indexes, params);
  return;
};


function sortData1(objA, stop, numR) {
  var sd = [];
  var indA = 0;
  var int1 = 60000; //  1 minuts
  for (var i = 0; i < numR; i++) {
    var dateTime;
    var dataRow = [];
    dateTime = stop - i * int1;
    dataRow[0] = U2Gtime(dateTime);
    if (objA[indA][0] < dateTime) {
      dataRow[1] = objA[indA][2];
      dataRow[2] = objA[indA][2];
      dataRow[3] = objA[indA][2];
      dataRow[4] = objA[indA][2];
      dataRow[5] = 0;
    } else {
      dataRow[1] = objA[indA][1];
      dataRow[2] = objA[indA][3];
      dataRow[3] = objA[indA][4];
      dataRow[4] = objA[indA][2];
      dataRow[5] = objA[indA][5];
      indA++;
    };
    sd[numR - i - 1] = dataRow;
  }
  return sd;
};

//*** Print data functions ***//

// print input data

function printInput1(sData) {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("1M");
  sheet.getRange('A3:F15003').clear();
  var n = sData.length
  sheet.getRange(sheeftR, 1, n, 6).setValues(sData);
};

// calc and print candels
function calcPrintCandels1() {
  var params = paramsFromMain();
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("1M");
  var sData = sheet.getRange(sheeftR + params.TOT - params.LIM, 1, params.LIM + 2, 6).getValues();
  var interval = params.INT;
  var cands = [];
  var n = 0,
    l = 0;
  for (var m = 0; m < sData.length; m++) {
    if (n == 0) {
      var cs = [0, 0, 0, 1000000, 0, 0, 0];
      cs[1] = sData[m][1];
    };
    cs[2] = cs[2] > sData[m][2] ? cs[2] : sData[m][2];
    cs[3] = cs[3] < sData[m][3] ? cs[3] : sData[m][3];
    cs[5] = cs[5] + sData[m][5];
    cs[6] = cs[6] + sData[m][1] > sData[m][4] ? -sData[m][5] : sData[m][5];
    n++;
    if (n == interval) {
      cs[0] = sData[m][0];
      cs[4] = sData[m][4];
      cands[l] = cs;
      l++;
      n = 0;
    };
  };
  sData = null;
  sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Cands");
  sheet.getRange('A3:G4303').clear();
  sheet.getRange(sheeftR, 1, l, 7).setValues(cands);
};

function calcPrintInd1() {
  var params = paramsFromMain();
  var numR = params.LIM / params.INT;
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Cands");
  var cands = sheet.getRange(sheeftR, 1, numR, 7).getValues();
  var inds = tradeIdx(cands, params);
  sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Ind");
  sheet.getRange('A3:M4303').clear();
  sheet.getRange('A1').setValue(inds.length);
  sheet.getRange(sheeftR, 1, numR, 13).setValues(inds);
};

function tradeIdx(cs, param) {
  var n = param.LIM / param.INT
  , x = param.SP + 1
  , pPow = param.PPOW
  , balance = param.BAL
  , kpi = param.KPI
  , margin = param.KT
  , bal = 0
  , feem = param.FEE / 100
  , str = ''
  , sum = 0
  , sum1 = 0
  , profit = 0
  , inds = [];
  
  for (var i = 0; i < n; i++) {
    inds[i] = [0,0,0,0,0,0,0,0,0,0,0,0,0];
    inds[i][0] = cs[i][0];
    inds[i][1] = cs[i][4];
 
   //   if (i > param.SP) {
/*      inds[i][2] = cs[i][4] - polinom1(cs, 4, 0, i, param.SP);
      inds[i][3] = param.KZ * inds[i-1][2];
      inds[i][4] = (inds[i][2]-inds[i][3]) > param.KD;
      inds[i][5] = inds[i][3] - inds[i][2] > 0;
      if (str == '' && inds[i][4]) {
      str = 'buy'
      bal = balance * kpi;
      balance = balance * (1-kpi) - bal * feem;
      sum = bal / cs[i][4];
      inds[i][6] = sum;
      inds[i][9] = balance;
      inds[i][10] = balance + bal;
      inds[i][11] = bal;
     }
     if (str == 'buy' && inds[i][5]) {
      str = 'sell';
      if (margin > 1) {
        bal = sum * cs[i][4] * feem;
        balance = balance + bal;
        profit = balance - param.BAL;
        sum = 0;
        inds[i][6] = sum;
        inds[i][8] = profit;
        bal = balance * margin * kpi;
        balance = balance + bal - bal * feem;
        sum1 = bal / cs[i][4];
        inds[i][7] = sum1;
        inds[i][9] = balance;
      } else {
        sum1 = sum * (1-kpi);
        bal = sum * kpi * cs[i][4] * (1-feem);
        balance = balance + bal;
        profit = balance + sum1*cs[i][4] - param.BAL;
        inds[i][6] = sum1;
        inds[i][8] = profit;
        inds[i][9] = balance;
        inds[i][10] = balance + sum1*cs[i][4];
        inds[i][11] = bal;
      }
    }
      if (str == 'sell' && inds[i][4]) {
        str = 'buy';
        if (margin > 1) {
          bal = sum1 * cs[i][4];
          balance = balance - bal - bal * feem;
          profit = balance - param.BAL;
          sum = 0;
          inds[i][7] = sum;
          inds[i][8] = profit;
          bal = balance * margin * kpi;
          balance = balance * (1 - kpi) - bal * feem;
          sum = bal / cs[i][4];
          inds[i][6] = sum;
          inds[i][9] = balance;
        } else {
          bal = balance * kpi;
          balance = balance * (1-kpi) - bal * feem;
          sum = sum1 + bal / cs[i][4];
          sum1 = 0;
          inds[i][6] = sum;
          inds[i][9] = balance;
          inds[i][10] = balance + sum * cs[i][4];
          inds[i][11] = bal;
        }
     }
  }
      for (k = 0; k <= pPow; k++) {
        inds[i][2 + k] = cs[i][2] - polinom1(cs, 2, k, i, param.SP);
      };
      for (k = 0; k <= pPow; k++) {
        inds[i][3 + pPow + k] = cs[i][3] - polinom1(cs, 3, k, i, param.SP);
      };
      for (k = 0; k <= pPow; k++) {
        inds[i][4 + 2*pPow + k] = cs[i][4] - polinom1(cs, 4, k, i, param.SP);
      };
    };
    if (i >= param.SP + param.D) {
      inds[i][11] = trend1(cs, 4, i, param.TR, 30 / param.INT);
      inds[i][12] = volot1(cs, i, param.VLT);
    };
*/     if (i > param.SP) {
      s = polinom1(cs, 4, 0, i-1, param.TR);
      inds[i][2] = cs[i][4] - s[0];
      s = polinom1(cs, 4, 1, i-1, param.TR);
      inds[i][3] = cs[i][4] - s[0];
      inds[i][4] = cs[i][4] - s[0] - s[1]*x;
      s = polinom1(cs, 4, 0, i-1, param.SP);
      inds[i][5] = cs[i][4] - s[0];
      s = polinom1(cs, 4, 1, i-1, param.SP);
      inds[i][6] = cs[i][4] - s[0];
      inds[i][7] = cs[i][4] - s[0] - s[1]*x;
      inds[i][8] = trend1(cs, 4, i, param.TR, 30 / param.INT);
      inds[i][9] = trend1(cs, 4, i, param.SP, 30 / param.INT);
      inds[i][10] = volot1(cs, i, 9);
      inds[i][11] = volot1(cs, i, param.SP);
    };
  /*    if (i >= param.SP + param.D) {

      for (k = 0; k <= pPow; k++) {
        inds[i][2 + k] = s[k];
        inds[i][6] -= s[k] * Math.pow(x, k);
      };
inds[i][7] = sma2(inds, 6, i, param.D);
      inds[i][8] = inds[i][6] - inds[i][7];
      d2[i] = Math.pow(inds[i][8], 2);
      inds[i][9] = stdev1(d2, i, param.D, param.D + param.SP);
      inds[i][10] = trend1(cs, 4, i, param.TR, 30 / param.INT);
      inds[i][11] = sma2(cs, 6, i, param.SP);
      inds[i][12] = volot1(cs, i, param.VLT);
    };
*/  };
  return inds;
};
  
function volot1(x, s, p) {
  if (s < p) return 0;
  var high = 0;
  var low = 1000000
  for (var i = 0; i < p; i++) {
    high = high > x[s - i][2] ? high : x[s - i][2];
    low = low < x[s - i][3] ? low : x[s - i][3];
  }
  return (high / low - 1) * 100;
};

function linest4(a, key, n, int) {
  var s1 = 0;
  var max = 0;
  var p1 = 0;
  for (var m = 1; m < int + 1; m++) p1 = p1 + a[n - int + m][key];
  return { k: p1 / int, s: a[n][key] - p1 / int };
};

function linest5(a, key, n, int) {
  var p1 = 0
    , p2 = 0
    , p3 = 0
    , p4 = 0;
  for (var m = 0; m < int; m++) {
    p1 = p1 + (n - m) * (n - m);
    p2 = p2 + a[n - m][key] * (n - m);
    p3 = p3 + (n - m);
    p4 = p4 + a[n - m][key];
  };
  var k = (p2 - p3 * p4 / int) / (p1 - p3 * p3 / int);
  var b = (p4 - k * p3) / int;
  var s = a[n][key] - k * n - b;
  return { s: s, k: k, b: b };
};

function polinom1(y, key, K, n, pts) {   
//  y - two-dimensional array of function values, key - second array key, K - polynomial power
//, pts - number of points, n - start position in array. K < pts!!!
    var i,j,k,s,M;
    var a = [], b = [], sums = [];
	//  calc coefficients of system of equations 
   	for(i=0; i<K+1; i++) {
      sums[i] = [0, 0, 0, 0, 0];
     		for(j=0; j<K+1; j++) {
        for(k=0; k<pts; k++) sums[i][j]+=Math.pow(k, i+j);
	     	}
   	}
	//  feel column of free polinomial members
  	for(i=0; i<K+1; i++) {
      b[i]= 0;
      if (key !=0) for(k=0; k<pts; k++) b[i] +=Math.pow(k,i)*y[n - pts + k + 1][key]
      else for(k=0; k<pts; k++) b[i] +=Math.pow(k,i)*(y[n - pts + k + 1][2]+y[n - pts + k + 1][3]+y[n - pts + k + 1][4])/3;
    }
	//  Gauss method to reduce the matrix to a triangular form
    for(k=0; k<K+1; k++) {
        for(i=k+1; i<K+1; i++) {
	        M=sums[i][k]/sums[k][k];
          b[i] -= M*b[k];
	        for(j=k; j<K+1; j++) sums[i][j] -= M * sums[k][j];
        }
    }
	//  calc coefficients of the approximating polynomial
    for(i=K;i>=0;i--) {
        s=0;
        for(j = i + 1; j<K+1; j++) s += sums[i][j]*a[j];
        a[i] = (b[i]-s)/sums[i][i];
    }
    return a;
};
 
function sma2(x, key, s, p) {
  var sum = 0;
  for (var i = 0; i < p; i++) {
    sum = sum + x[s - i][key];
  }
  return sum / p;
};

function stdev1(x, s, p, int) {
  if (s < p + int) return null;
  return Math.sqrt(sma1(x, s, p));
};

function sma1(x, s, p) {
  var sum = 0;
  for (var i = 0; i < p; i++) {
    sum = sum + x[s - i];
  }
  return sum / p;
};

function median1(x, key, s, p, int) {
  if (s < p + int) return null;
  var med = 0;
  for (var k = 0; k < p; k++) med = med + (x[s - k][key] > 0 ? x[s - k][key] : -x[s - k][key]);
  return med / p;
};

function trend1(a, key, start, n, k) {
  var s0 = 0,
    s1 = 0,
    s2 = 0,
    s3 = 0,
    s4 = 0;
  var base = a[start - n + 1][key];
  for (var i = 0; i < n; i++) {
    s4 = (a[start - n + i + 1][key] / base - 1) * 100 * k;
    s0 = s0 + i;
    s1 = s1 + s4;
    s2 = s2 + i * s4;
    s3 = s3 + i * i;
  }
  return (n * s2 - s0 * s1) / (n * s3 - Math.pow(s0, 2))
};


//*** Data load and Bitfinex API functions ***//

function getSupprotedSymbols() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Dict");
  var sheeftR1 = 2;
  var colToJumpTo = 1;
  var url = "https://api.bitfinex.com/v1/symbols/"
  var response = UrlFetchApp.fetch(url);
  var tickers = Utilities.jsonParse(response.getContentText());
  for (i = 0; i < tickers.length; i++) {
    sheet.getRange(sheeftR1 + i, colToJumpTo).setValue(tickers[i].toUpperCase());
  }
};

function getBitfinexData(symbol, stop, interval) {
  var startDATE;
  var stopDATE1 = stop;
  var objA = [];
  var objA1 = {};
  for (var ii = 15; ii > 0; ii--) {
    if (interval > 1000) {
      startDATE = stopDATE1 - 60000000 + 60000;
      var url = "https://api.bitfinex.com/v2/candles/trade:1m:t" + symbol + "/hist?limit=1000&start=" + startDATE + "&end=" + stopDATE1;
      var response = UrlFetchApp.fetch(url);
      var objA1 = Utilities.jsonParse(response.getContentText());
      for (var i = 0; i < objA1.length; i++) objA[objA.length] = objA1[i]
      Utilities.sleep(335);
      interval = interval - 1000;
      stopDATE1 = stopDATE1 - 60000000;
    } else if (interval >= 0) {
      var url = "https://api.bitfinex.com/v2/candles/trade:1m:t" + symbol + "/hist?limit=" + interval.toString() + "&end=" + stopDATE1;
      var response = UrlFetchApp.fetch(url);
      var objA1 = Utilities.jsonParse(response.getContentText());
      for (var i = 0; i < objA1.length; i++) objA[objA.length] = objA1[i]
      Utilities.sleep(335);
      interval = interval - 10000
    };
  };
  return objA;
};

function paramsFromMain() {
  var params = {};
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Main");
  params.DATE = sheet.getRange(3, 1).getValue();
  params.INT = sheet.getRange(3, 2).getValue();
  var days = sheet.getRange(3, 3).getValue();
  params.A = sheet.getRange(3, 4).getValue();
  params.B = sheet.getRange(3, 5).getValue();
  params.CVR = sheet.getRange(7, 1).getValue();
  params.SP = sheet.getRange(7, 2).getValue();
  params.M = sheet.getRange(7, 3).getValue();
  params.D = sheet.getRange(7, 4).getValue();
  params.TR = sheet.getRange(7, 5).getValue();
  params.VLT = sheet.getRange(7, 6).getValue();
  switch (days) {
    case "12h": { params.LIM = 0.5; break };
    case "1d": { params.LIM = 1; break };
    case "2d": { params.LIM = 2; break };
    case "3d": { params.LIM = 3; break };
    case "4d": { params.LIM = 4; break };
    case "5d": { params.LIM = 5; break };
    case "6d": { params.LIM = 6; break };
    case "7d": { params.LIM = 7; break };
    case "2W": { params.LIM = 14; break };
    case "1M": { params.LIM = 30; break };
    default: { params.LIM = 1; break };
  };
  params.LIM = params.LIM * 1440 + params.INT * (params.SP + params.D * 2 + params.M);
  params.TOT = sheet.getRange(4, 7).getValue();
  sheet.getRange(3, 7).setValue(params.LIM);
  sheet.getRange(3, 8).setValue(params.LIM / params.INT);
  params.AF = sheet.getRange(11, 1).getValue();
  params.MAX = sheet.getRange(11, 2).getValue();
  params.SAR = sheet.getRange(11, 3).getValue();
  params.TRDA = sheet.getRange(11, 4).getValue();
  params.TRDB = sheet.getRange(11, 5).getValue();
  params.PPOW = sheet.getRange(11, 6).getValue();
  params.FEE = sheet.getRange(15, 1).getValue();
  params.IN = sheet.getRange(15, 2).getValue();
  params.KZ = sheet.getRange(15, 3).getValue(); //  number stdev or median
  params.KD = sheet.getRange(15, 4).getValue(); //  min stdev or median level for out
  params.OUT = sheet.getRange(15, 5).getValue(); //  out type
  params.BAL = sheet.getRange(17, 1).getValue(); //  Start balance in USD
  params.KPI = sheet.getRange(17, 2).getValue(); //  Procent of balance for trade
  params.REF = sheet.getRange(17, 3).getValue(); //  Parameter of refinance
  params.KT = sheet.getRange(17, 4).getValue(); //  trend level
  return params;
};

//*** Service functions ***//

function U2Gtime(unixtime) {
  if (unixtime != 0) {
    var newDate = new Date();
    newDate.setTime(unixtime);
    dateString = newDate.toISOString().
      replace(/T/, ' '). // replace T with a space
      replace(/\..+/, ''); // delete the dot and everything after
    return dateString
  } else return 0;
};

function Unixtime(u2Gtime) {
  var unixTime = new Date(Date.parse(u2Gtime)).getTime();
  return unixTime;
};

