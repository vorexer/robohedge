module.exports = function trend (s, key, length, koeff, source_key) {
  if (!source_key) source_key = 'close'
  if (s.lookback.length >= length) {
    var prev_ema = s.lookback[0][key]
    , base = s.lookback[length-1][source_key]
    , s0 = 0
    , s1 = 0
    , s2 = 0
    , s3 = 0
    , s4 = 0
    , i = 0;    
    s.lookback.slice(0, length).forEach(function (period) {
        s4 = (period[source_key]/base - 1) * 100 * koeff
        s0 +=i
        s1 += s4
        s2 += i*s4
        s3 += i*i
        i++
    })
    s4 = (s.period[source_key]/base - 1) * 100 * koeff
    s0 +=i
    s1 += s4
    s2 += i*s4
    s3 += i*i
    s.period[key] = -(length * s2 - s0 * s1) / (length * s3 - Math.pow(s0, 2))
  }
}

