module.exports = function defineTrend (sobj) {
  if (sobj.lookback[sobj.options.bb5_trend_period * 2] && sobj.lookback[sobj.options.bb5_trend_period * 2].close) {
    var delta1 = (sobj.period.close - sobj.lookback[sobj.options.bb5_trend_period].close) / sobj.options.bb5_trend_period
    var delta2 = (sobj.lookback[sobj.options.bb5_trend_period].close - sobj.lookback[sobj.options.bb5_trend_period * 2].close) / sobj.options.bb5_trend_period
    var trend = (sobj.period.close - sobj.lookback[sobj.options.bb5_trend_period * 2].close) / sobj.options.bb5_trend_period / 2
    trend += delta1 - delta2
    sobj.bb5.trend = trend < -sobj.options.bb5_neutral_trend ? 'down' : (trend > sobj.options.bb5_neutral_trend ? 'up' : 'none')
    sobj.bb5.trend_value = trend
  }
}

