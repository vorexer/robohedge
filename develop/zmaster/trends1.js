module.exports = function defineTrend (sobj) {
  var n = sobj.options.bb5_trend_period
  if (sobj.lookback[n-1] && sobj.lookback[n-1].close) {
    var s0 = 0
      , s1 = 0
      , s2 = 0
      , s3 = 0
      , s4 = 0
      , base = sobj.lookback[n-1].close
    for (var i = 0; i < n; i++) {
      s4 = (sobj.lookback[n-i-1].close / base - 1) * 100
      s0 = s0 + i
      s1 = s1 + s4
      s2 = s2 + i * s4
      s3 = s3 + i * i
    }
    sobj.bb5.trend_value = (n * s2 - s0 * s1) / (n * s3 - Math.pow(s0, 2))    
    sobj.bb5.trend = sobj.bb5.trend_value < -sobj.options.bb5_neutral_trend ? 'down' : (sobj.bb5.trend_value > sobj.options.bb5_neutral_trend ? 'up' : 'none')
  }
}
