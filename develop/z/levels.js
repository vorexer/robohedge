module.exports = function defineLevel (sobj) {
    if (!sobj.bb5) sobj.bb5 = {
        levelWidth: 0
        , maxLevel: -5
        , minLevel: 5
        , level: 0
        , lastLevel: 0
        , signal: 'none'
        , trend: 'none'
        , maxPrice: 0
        , minPrice: 100000
    }
    if (sobj.period.sma && sobj.period.stdev) {
        var cross_up, cross_down
        //  define level width
        sobj.bb5.levelWidth = sobj.period.stdev / sobj.period.sma
        //  define level logic
        if (sobj.period.close / sobj.options.bb5_cross_down_pct < sobj.period.sma) {
            for (var i = 0; i < 4; i++) {
                if ((sobj.period.close / sobj.options.bb5_cross_down_pct - sobj.period.sma) < -sobj.period.stdev * sobj.options.bb5_time * Math.pow(sobj.options.bb5_fibo, i)) break
            }
            sobj.bb5.level = i - 5
            if (sobj.bb5.level > sobj.bb5.lastLevel) {
                if ((sobj.period.close * sobj.options.bb5_cross_down_pct - sobj.period.sma) < -sobj.period.stdev * sobj.options.bb5_time * Math.pow(sobj.options.bb5_fibo, i - 1)) {
                    sobj.bb5.level -= 1
                }
            }
        } else if (sobj.period.close * sobj.options.bb5_cross_up_pct > sobj.period.sma) {
            for (var i = 0; i < 4; i++) {
                if ((sobj.period.high * sobj.options.bb5_cross_up_pct - sobj.period.sma) > sobj.period.stdev * sobj.options.bb5_time * Math.pow(sobj.options.bb5_fibo, i)) break
            }
            sobj.bb5.level = 5 - i
            if (sobj.bb5.level < sobj.bb5.lastLevel) {
                if ((sobj.period.close / sobj.options.bb5_cross_up_pct - sobj.period.sma) > sobj.period.stdev * sobj.options.bb5_time * Math.pow(sobj.options.bb5_fibo, i - 1)) {
                    sobj.bb5.level += 1
                }
            }
        }
        //  other level logic
        if (Math.abs(sobj.bb5.level) == 5) {
            sobj.bb5.minLevel = sobj.bb5.level
            sobj.bb5.maxLevel = sobj.bb5.level
        } else {
            sobj.bb5.minLevel = Math.min(sobj.bb5.minLevel, sobj.bb5.level)
            sobj.bb5.maxLevel = Math.max(sobj.bb5.maxLevel, sobj.bb5.level)
        }
        // max/min price logic
        sobj.bb5.minPrice = Math.min(sobj.bb5.minPrice, sobj.period.close)
        sobj.bb5.maxPrice = Math.max(sobj.bb5.maxPrice, sobj.period.close)
    }
}

