var z = require('zero-fill')
  , n = require('numbro')
  , trend = require('../../../lib/trend')
  , Phenotypes = require('../../../lib/phenotype')

module.exports = {
  name: 'trend',
  description: 'Buy when (TREND < -0.5) and sell when (TREND > 0.5).',

  getOptions: function () {
    this.option('period', 'period length, same as --period_length', String, '3m')
    this.option('period_length', 'period length, same as --period', String, '3m')
    this.option('min_periods', 'min. number of history periods', Number, 40)
    this.option('trend_period', 'number of periods for the TREND', Number, 10)
    this.option('trend_koeff', 'multiplier for 30m period', Number, 10)
    this.option('up_trend_threshold', 'threshold to trigger a buy signal', Number, 0.5)
    this.option('down_trend_threshold', 'threshold to trigger a sold signal', Number, 0.4)
    this.option('signal_pct', 'price deviation to trigger a trade signal', Number, 0.4)
  },

  calculate: function (s) {

    // compute TREND
    trend(s, 'trend', s.options.trend_period, s.options.trend_koeff)
  },

  onPeriod: function (s, cb) {
    if (typeof s.period.trend === 'number') {
      console.log('----------------------------------------------------')
      if (!s.signal && s.period.trend < -s.options.down_trend_threshold) s.pre_signal = 'buy'
      else if (!s.signal && s.period.trend > s.options.up_trend_threshold) s.pre_signal = 'sell'
      if (!s.api_order && s.pre_signal === 'buy') {
        s.min_price = Math.min(s.min_price, s.period.close)
        if (s.period.close > s.min_price * (1 + s.options.signal_pct/100)) {  
          s.signal = 'buy'
          s.pre_signal = ''
        }
      } else if (!s.api_order && s.pre_signal === 'sell') {
        s.max_price = Math.max(s.max_price, s.period.close)
        if (s.period.close < s.max_price / (1 + s.options.signal_pct/100)) {  
          s.signal = 'sell'
          s.pre_signal = ''
        }
      } else {
        s.signal = null  // hold
        s.min_price = 1000000
        s.max_price = 0
      }
    }
    cb()
  },

  onReport: function (s) {
    var cols = []
    if (typeof s.period.trend === 'number') {
      var color = 'grey'
      if (s.period.trend > s.options.up_trend_threshold) {
        color = 'green'
      }
      else if (s.period.trend < -s.options.down_trend_threshold) {
        color = 'red'
      }
      cols.push(z(8, n(s.period.trend).format('+00.0000'), ' ')[color])
      cols.push(z(8, n(s.period.overbought_rsi).format('00'), ' ').cyan)
    }
    else {
      cols.push('         ')
    }
    return cols
  },

  phenotypes: {
    // -- common
    period_length: Phenotypes.RangePeriod(1, 120, 'm'),
    min_periods: Phenotypes.Range(1, 200),
    markdown_buy_pct: Phenotypes.RangeFloat(-1, 5),
    markup_sell_pct: Phenotypes.RangeFloat(-1, 5),
    order_type: Phenotypes.ListOption(['maker', 'taker']),
    sell_stop_pct: Phenotypes.Range0(1, 50),
    buy_stop_pct: Phenotypes.Range0(1, 50),
    profit_stop_enable_pct: Phenotypes.Range0(1, 20),
    profit_stop_pct: Phenotypes.Range(1,20),

    // -- strategy
    trend_period: Phenotypes.Range(2, 20),
    up_trend_threshold: Phenotypes.RangeFloat(-1, 3),
    down_trend_threshold: Phenotypes.RangeFloat(-1, 3),
    trend_koeff: Phenotypes.Range(1, 30),
    signal_pct: Phenotypes.RangeFloat(-1, 5)
  }
}

