// BCH settings

var c = module.exports = {}

c.selector = 'bitfinex.BCH-USD'
c.strategy = 'boba5'
c.period = '2m'
c.min_periods = 500
c.asset_capital = 0
c.currency_capital = 1000

// Optional stop-order triggers:
// sell if price drops below this % of bought price (0 to disable)
c.sell_stop_pct = 3
// buy if price surges above this % of sold price (0 to disable)
c.buy_stop_pct = 0
// enable trailing sell stop when reaching this % profit (0 to disable)
c.profit_stop_enable_pct = 5
// maintain a trailing stop this % below the high-water mark of profit
c.profit_stop_pct = 0.5

// Order execution rules:
// avoid trading at a slippage above this pct
c.max_slippage_pct = 10
// buy with this % of currency balance (WARNING : sim won't work properly if you set this value to 100)
c.buy_pct = 99.5
// sell with this % of asset balance (WARNING : sim won't work properly if you set this value to 100)
c.sell_pct = 99.5

// boba5 strategy settings:
c.bb5_size = 420
c.bb5_time = 2.618
c.bb5_cross_up_pct = 0.995
c.bb5_cross_down_pct = 0.995
c.bb5_fibo = 0.618
c.bb5_level_width = 0.01
c.bb5_level_down_sell = 2
c.bb5_level_up_buy = 3
c.bb5_trend_periods = 10
c.bb5_neutral_trend = 0.1
c.bb5_debug = true
